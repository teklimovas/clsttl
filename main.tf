terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = "AQAAAABeQ5ByAATuwXxAssNFdsfdgsfdgg"
  cloud_id  = "b1gmdnbp9d367d8ddlbt"
  folder_id = "b1gtlfep5fqq478m4mr5"
  zone      = "ru-central1-a"
}




resource "yandex_compute_instance" "vm-1" {
  name = "clst1"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd88d14a6790do254kj7"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "centos:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDVd2Z2Kdc70bwstMw5koQ9i0H0nOO8V+UZTk+1Wl25wyRU2VY+UmYvjnvKcAwD/1GQ33hzhrtxtylDM01VPfG9vfc6Mix7FWgKjRFXFqovUJtkpX+bzAQFUJWMbaqL7WkQcdxThz/4f4EZDd4R0ZhB90NRM3EhBLcT8AEKJINBcMNupTLaBaBQ7Tb267nwBd7BWmn4Ds0dQ64e0Q8zcYCy5hdkTWq0XB7wWo5N+2UG0ar3Ul4oOhgYq//QT2JXYqfiDWXgZKpJFsRbuK1rFwvWRRC/n+46kSoDgBEJtcXhVsmGBxy+zS2ggSlbX4q8DAtl2yD3MDsG4f6tgE7EyuUp root@klimov.com"
  }
}

resource "yandex_compute_instance" "vm-2" {
  name = "clst2"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd88d14a6790do254kj7"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "centos:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDVd2Z2Kdc70bwstMw5koQ9i0H0nOO8V+UZTk+1Wl25wyRU2VY+UmYvjnvKcAwD/1GQ33hzhrtxtylDM01VPfG9vfc6Mix7FWgKjRFXFqovUJtkpX+bzAQFUJWMbaqL7WkQcdxThz/4f4EZDd4R0ZhB90NRM3EhBLcT8AEKJINBcMNupTLaBaBQ7Tb267nwBd7BWmn4Ds0dQ64e0Q8zcYCy5hdkTWq0XB7wWo5N+2UG0ar3Ul4oOhgYq//QT2JXYqfiDWXgZKpJFsRbuK1rFwvWRRC/n+46kSoDgBEJtcXhVsmGBxy+zS2ggSlbX4q8DAtl2yD3MDsG4f6tgE7EyuUp root@klimov.com"
  }
}

resource "yandex_compute_instance" "vm-3" {
  name = "clst3"

  resources {
    cores  = 2
    memory = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd88d14a6790do254kj7"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "centos:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDVd2Z2Kdc70bwstMw5koQ9i0H0nOO8V+UZTk+1Wl25wyRU2VY+UmYvjnvKcAwD/1GQ33hzhrtxtylDM01VPfG9vfc6Mix7FWgKjRFXFqovUJtkpX+bzAQFUJWMbaqL7WkQcdxThz/4f4EZDd4R0ZhB90NRM3EhBLcT8AEKJINBcMNupTLaBaBQ7Tb267nwBd7BWmn4Ds0dQ64e0Q8zcYCy5hdkTWq0XB7wWo5N+2UG0ar3Ul4oOhgYq//QT2JXYqfiDWXgZKpJFsRbuK1rFwvWRRC/n+46kSoDgBEJtcXhVsmGBxy+zS2ggSlbX4q8DAtl2yD3MDsG4f6tgE7EyuUp root@klimov.com"
  }
}

resource "yandex_compute_instance" "vm-4" {
  name = "mon"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd88d14a6790do254kj7"
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "centos:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDVd2Z2Kdc70bwstMw5koQ9i0H0nOO8V+UZTk+1Wl25wyRU2VY+UmYvjnvKcAwD/1GQ33hzhrtxtylDM01VPfG9vfc6Mix7FWgKjRFXFqovUJtkpX+bzAQFUJWMbaqL7WkQcdxThz/4f4EZDd4R0ZhB90NRM3EhBLcT8AEKJINBcMNupTLaBaBQ7Tb267nwBd7BWmn4Ds0dQ64e0Q8zcYCy5hdkTWq0XB7wWo5N+2UG0ar3Ul4oOhgYq//QT2JXYqfiDWXgZKpJFsRbuK1rFwvWRRC/n+46kSoDgBEJtcXhVsmGBxy+zS2ggSlbX4q8DAtl2yD3MDsG4f6tgE7EyuUp root@klimov.com"
  }
}


resource "yandex_vpc_network" "network-1" {
  name = "network1"
}


resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["10.0.0.0/24"]
}

output "internal_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "internal_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.ip_address
}

output "internal_ip_address_vm_3" {
  value = yandex_compute_instance.vm-3.network_interface.0.ip_address
}

output "internal_ip_address_vm_4" {
  value = yandex_compute_instance.vm-4.network_interface.0.ip_address
}

output "external_ip_address_vm_1" {
  value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

output "external_ip_address_vm_2" {
  value = yandex_compute_instance.vm-2.network_interface.0.nat_ip_address
}

output "external_ip_address_vm_3" {
  value = yandex_compute_instance.vm-3.network_interface.0.nat_ip_address
}

output "external_ip_address_vm_4" {
  value = yandex_compute_instance.vm-4.network_interface.0.nat_ip_address
}